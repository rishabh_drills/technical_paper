# Technical_paper on OOPs.

## Introduction

Object-Oriented Programming is a way of writing code that allows you to create different objects from a common object. The common object is usually called a blueprint while the created objects are called instances. Each instance has properties that are not shared with other instances.

Programmers eventually discovered that it makes a program clearer and easier to understand if they were able to take a bunch of data and group it together with the functions that worked on that data. Such a grouping data and functions are called class and object. And writing programs by using classes is known as object oriented programming.

## Features of Object Oriented Programming

###### Class

A class is a user defined data type which contains data members and member function to operate on those data member. It is a collection of similar kind of objects. A class is a generic definition of an object. It is a blue print of an object. Class is the very essential part of object-oriented programming. The class is used to implement encapsulation, data abstraction, and data hiding.
Syntax for class :-

A class Example:-

class User {
  #password;
  constructor(name, userName, password) {
    this.name = name;
    this.userName = userName;
    this.#password = password;
  }

  login(userName, password) {
    if (userName === this.userName && password === this.#password) {
      console.log('Login Successfully');
    } else {
      console.log('Authentication Failed!!');
    }
  }

  setPassword(newPassword) {
    this.#password = newPassword;
  }
};

const nehal = new User('Nehal Mahida', 'nehal_mahida', 'password:)');
const js = new User('JavaScript', 'js', 'python:)');


nehal.login('nehal_mahida', 'password:)'); // Login Successfully
js.login('js', 'python:)'); // Login Successfully

console.log(nehal.name); // Nehal Mahida
console.log(nehal.password); // undefined
console.log(nehal.#password); // Syntax Error

nehal.setPassword('new_password:)');
nehal.login('nehal_mahida', 'password:)'); // Authentication Failed!!
nehal.login('nehal_mahida', 'new_password:)'); // Login Successfully

###### Object

Objects are basic run time entities in an object-oriented programming. Each object contains data and code to manipulate that data. Objects can have interaction barring having to understand details about the statistics or code. In structured programming a problem is approached by using dividing it into functions.

const user = {
  name: 'Nehal Mahida',
  userName: 'nehal_mahida',
  password: 'password:)',
  login: function(userName, password) {
    if (userName === this.userName && password === this.password) {
      console.log('Login Successfully');
    } else {
      console.log('Authentication Failed!!');
    }
  },
};


user.login('nehal', 'nehal');
user.login('nehal_mahida', 'password:)');

// Authentication Failed!!
// Login Successfully




## Application of OOPS

Object oriented programming has application in many areas most popular application of object-oriented programming now has been in the area of interface design such as windows. There are thousands of windowing system developed using objects-oriented techniques.

 Modeling and Simulation.
 Object Oriented Database.
 Hypertext and Hypermedia.
 Artificial Intelligence and Expert System.
 Parallel Programming and Neural Networks.
 Decision support and Office Automation Systems.

## Advantages of OOPs 

OOP offers several benefits to the programmers as well as the user. Some advantages are as follows:
 Security is the first main advantage of OOP. The data and functions are combined together in the
  form of class.
 Using inheritance we can eliminate redundant code and extend the use of existing class.
 We can build programs from the standard working modules that communicate with one another. This leads to saving of  development time and increase high productivity.
 The concept of data hiding helps the programmer to build secure programs, so that data of one class will be secure from   the other parts of the program.
 Software complexity can be easily managed.

## Disadvantages of OOPS

The OOP languages have the following disadvantages.
 The message base communication between many objects in a complex system is difficult to trace and debug.
 It requires more memory to process at a great speed.
 It runs at slower than the traditional programming languages.
 It works on objects and everything of the real world is not possible to divide into neat classes and subclasses.

# Conclusion

We have to study in above research paper, what exactly the object-oriented programming (OOPs) is, there features and we also study the use of OOPs in future. The programming languages before OOP system were not easy to understand. The large code converts into short code by using this OOPs concept. With the use of feature like class, objects, encapsulations,polymorphism, inheritance, and abstraction it can be seen that development of software is increase by using these capabilities.

## References

 Mr. Rushikesh S Raut , Research Paper on Object Oriented Programming , International Research Journal of Engineering andTechnology (IRJET) e-ISSN: 2395-0056  Volume: 07 Issue: 10 | Oct 2020
 Simple learn - https://www.simplilearn.com/tutorials/javascript-tutorial/oop-in-javascript.
 FreeCodeCamp - https://www.freecodecamp.org/news/how-javascript-implements-oop/
 MarkDown Format - https://guides.github.com/features/mastering-markdown/



